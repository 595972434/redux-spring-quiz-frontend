export const getProductList = () => (dispatch) => {
    const url = `http://localhost:8080/api/products`;
    fetch(url, {method: 'GET'}).then(response => response.json()).then(
        data => {
            console.log(data)
            dispatch({
                type: 'GET_PRODUCT_LIST',
                productList: data
            });
        }
    )
};
