export const postProduct = (product) => (dispatch) => {
    const url = `http://localhost:8080/api/product`;
    let body = {
        "productName": product.productName,
        "productPrice": product.productPrice,
        "productUnit": product.productUnit,
        "productImage": product.productImage
    };

    console.log(body);
    fetch(url, {method: 'POST', body: JSON.stringify(body), headers: {'Content-Type': 'application/json'}})
        .then(response => response.json())
        .then(
            (data) => dispatch({
                type: 'POST_PRODUCT',
                postResult: data
            })
        ).catch((data)=>console.log(data));

};
