export const updateOrderList = (orders) => (dispatch) => {
    dispatch({
        type: 'UPDATE_ORDER',
        orderList:orders
    })
};
