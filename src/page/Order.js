import React from "react";
import {getProductList} from "../action/GetProductAction";
import {updateOrderList} from "../action/UpdateOrderAction";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class Order extends React.Component{


    constructor(props) {
        super(props);
    }

    handleDelete(index){
        const orderInfo=this.props.orderList;
        orderInfo.splice(index,1);
        this.props.updateOrderList(orderInfo);
    }
    render() {
        console.log(this.props.orderList);
        return (
            <div>
                {this.props.orderList.map((item, index) => {
                    return (
                        <div key={index}>
                            <label >名称：{item.productName}   </label>
                            <label >价格：{item.productPrice}   </label>
                            <label >数量：{item.productNum}   </label>
                            <label >单位：{item.productUnit}   </label>
                            <button onClick={this.handleDelete.bind(this,index)}>删除</button>
                        </div>)

                })
                }
            </div>
        );
    }
}
const mapStateToProps = state => ({
    orderList: state.products.orderList
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProductList,
    updateOrderList
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Order);
