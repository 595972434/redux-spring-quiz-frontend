import React from "react";
import {getProductList} from "../action/GetProductAction";
import {postProduct} from "../action/PostProductAction";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class AddProduct extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isDisable: true,
            productName: "",
            productPrice: "",
            productUnit: "",
            productImage: ""
        };
        this.checkInputFormat = this.checkInputFormat.bind(this);
    }

    nameHandle(e) {
        this.state.productName = e.target.value;
        this.checkInputFormat();
    }

    priceHandle(e) {
        this.state.productPrice = e.target.value;
        this.checkInputFormat();
    }

    unitHandle(e) {
        this.state.productUnit = e.target.value;
        this.checkInputFormat();
    }

    imageHandle(e) {
        this.state.productImage = e.target.value;
        this.checkInputFormat();
    }

    submit() {
        this.props.postProduct(this.state);
        this.props.history.goBack();
    }

    checkInputFormat() {
        let bool=(this.state.productName.trim() === "" || this.state.productUnit === "" || this.state.productImage === "" || this.state.productPrice === "" ||(isNaN(this.state.productPrice)));
        this.setState(
            {isDisable:bool}
        )
    }
    render() {
        return (
            <div>
                <label>名称</label>
                <input onChange={this.nameHandle.bind(this)}/>
                <label>价格</label>
                <input onChange={this.priceHandle.bind(this)}/>
                <label>单位</label>
                <input onChange={this.unitHandle.bind(this)}/>
                <label>图片</label>
                <input onChange={this.imageHandle.bind(this)}/>
                <button disabled={this.state.isDisable} onClick={this.submit.bind(this)}> 提交</button>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    postResult: state.addResult.postResult
});

const mapDispatchToProps = dispatch => bindActionCreators({
    postProduct,
    getProductList
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);
