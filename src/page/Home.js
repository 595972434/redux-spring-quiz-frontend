import React from "react";
import {getProductList} from '../action/GetProductAction'
import {updateOrderList} from '../action/UpdateOrderAction'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
class Home extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            addToOrderList:[]
        }
    }

    componentDidMount() {
        this.props.getProductList();
    }
    componentWillUnmount() {
        this.props.updateOrderList(this.state.addToOrderList);
    }

    handleAdd(item){
        this.addToOrder(item);
    }
    addToOrder(item){
        const orderList=this.state.addToOrderList;
        let find=false;
        orderList.forEach((value,index)=>{
            if(item.productName===value.productName){
                value.productNum+=1;
                find=true;
            }
        });
        if(!find){
        orderList.push({
            productName:item.productName,
            productPrice:item.productPrice,
            productNum:1,
            productUnit:item.productUnit
        });
        }
        this.setState(
            {
                addToOrderList:orderList
            }
        )
    }

    render() {
        return(
        <div>
            {this.props.productList.map((item, index) => {
                return (
                    <div key={index}>
                        <label >名称：{item.productName}</label>
                        <label >价格：{item.productPrice}{item.productUnit}</label>
                        <button onClick={this.handleAdd.bind(this,item)}>添加</button>

                    </div>)

            })
            }
        </div>
        )
    }
}
const mapStateToProps = state => ({
    productList: state.products.productList
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProductList,
    updateOrderList
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
