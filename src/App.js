import React, {Component} from 'react';
import {BrowserRouter as Router,Link} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from './page/Home'
import Order from './page/Order'
import AddProduct from './page/AddProduct'

import './App.less';

class App extends Component{
  render() {
      return (

          <Router>
              <div>
                  <Link to={'/'}> 商城 </Link>
                  <Link to={'/orders'}> 订单 </Link>
                  <Link to={'/addProducts'}> 添加商品 </Link>
              </div>
              <Switch>
                  <Route exact path="/" component={Home}/>
                  <Route path='/orders' component={Order}/>
                  <Route path='/addProducts' component={AddProduct}/>
              </Switch>

          </Router>
          );
  }
}

export default App;
