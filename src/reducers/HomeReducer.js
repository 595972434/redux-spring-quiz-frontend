const initState = {
    productList: [],
    orderList: []
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_PRODUCT_LIST':
            return {
                ...state,
                productList: action.productList
            };
        case 'UPDATE_ORDER':
            return {
                ...state,
                orderList: action.orderList
            };

        default:
            return state
    }
};
