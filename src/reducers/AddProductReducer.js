const initState = {
    postResult: ""
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'POST_PRODUCT':
            return {
                ...state,
                postResult: action.postResult
            };

        default:
            return state
    }
};
