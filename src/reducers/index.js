import {combineReducers} from "redux";
import HomeReducer from './HomeReducer'
import PostProductReducer from './AddProductReducer'
const reducers = combineReducers({
        products:HomeReducer,
        addResult:PostProductReducer
});
export default reducers;
